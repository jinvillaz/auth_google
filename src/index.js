import { config } from './config';
import log4js from 'log4js';
import { createServer } from 'http';
import { configLogger } from './config/log4js';
import { App } from './app';

log4js.configure(configLogger);
const logger = log4js.getLogger('Index');

const runServer = async () => {
  try {
    const app = new App();
    await app.loadModules();
    const server = createServer(app.getApp());
    server.listen(config.PORT, () => {
      logger.info('Server listening on port:', config.PORT);
    });
  } catch (err) {
    logger.error(err.message);
    logger.error(err.stack);
    logger.error('cannot up server.');
  }
};
runServer();
