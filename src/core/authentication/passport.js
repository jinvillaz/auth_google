import log4js from 'log4js';
import to from 'await-to-js';
import { Strategy, ExtractJwt } from 'passport-jwt';
import GooglePlusTokenStrategy from 'passport-google-plus-token';
import { config } from '../../config';
import { User } from '../db/auth-model';

const logger = log4js.getLogger('PassportStrategy');

export class PassportStrategy {

  static getJWTStrategy() {
    return new Strategy({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: config.JWT_STRATEGY_SECRET
    }, async (payload, done) => {
      const [error, user] = await to(User.findById(payload.sub));
      if (error) {
        return done(error, false);
      }      
      if (!user) {
        return done(null, false);
      }      
      done(null, user);
    });
  }

  static getGoogleStrategy() {
    return new GooglePlusTokenStrategy({
      clientID: config.OUTH_GOOGLE_CLIENT_ID,
      clientSecret: config.OUTH_GOOGLE_CLIENT_SECRET,
      passReqToCallback: true
    }, async (req, accessToken, refreshToken, profile, next) => {
      //logger.info('profile', profile);
      logger.info('accessToken', accessToken);
      const [error, user] = await to(User.findOne({ 'data.id': profile.id }));
      logger.info('error, user', error, user);
      if (error) {
        return next(error);
      } 
      if (user) {
        return next(null, user);
      }
      const newUser = new User({
        method: 'google',
        data: {
          id: profile.id,
          email: profile.emails[0].value,
          picture: profile.photos[0].value
        },
        name: profile.displayName
      });
      await newUser.save();
      return next(null, newUser);    
    });    
  }
}
