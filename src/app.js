import path from 'path';
import express from 'express';
import log4js from 'log4js';
import cors from 'cors';
import morgan from 'morgan';
import errorHandler from 'errorhandler';
import bodyParser from 'body-parser';
import passport from 'passport';
import { PassportStrategy } from './core/authentication/passport';
import { DbConnection } from './core/db/connection';
import { Api } from './modules/routes/route-auth';

const logger = log4js.getLogger('App');

/**
 * Main app module.
 */
export class App {

  constructor() {
    this.db = new DbConnection();
    this.app = express();
    if (process.env.NODE_ENV === 'development') {      
      logger.info('configuration for develop.');
      this.app.use(errorHandler());
      this.app.use(morgan('dev'));
    } else {      
      logger.info('configuration for production.');
      this.app.use(morgan('combined'));
      this.app.use(express.static(path.resolve('dist/public')));
    }
    this.app.use(bodyParser.json({ limit: '50mb' }));
    this.app.use(bodyParser.urlencoded({ limit: '50mb', extended: false }));
    this.app.use(cors());    
    passport.use(PassportStrategy.getJWTStrategy());
    passport.use('googleToken', PassportStrategy.getGoogleStrategy());    
    this.app.use('/api/v1/auth', Api.registerModule());
    this.app.use((req, res, next) => {
      const err = new Error('Not found');
      err.status = 404;
      next(err);
    });
    this.app.use((err, req, res, next) => {// eslint-disable-line no-unused-vars
      if (process.env.NODE_ENV === 'development') {
        logger.error(err);
      } else {
        logger.error(err.message);
      }
      const status = err.status || 500;
      res.status(status).json({
        message: err.message
      });      
    });    
  }

  /**
   * Gets the app server.
   * @returns {Object} app server.
   */
  getApp() {
    return this.app;
  }

  /**
   * Loads modules.
   */
  async loadModules() {
    try {
      await this.db.connect();
      logger.info('loadModules..');
    } catch (error) {
      logger.error(error);
    }
  }
}
