import superagent from 'superagent';
import { config } from '../../config';

/**
 * Module for register api.
 */
class Images {

  static async uploadImage(req, res, next) {
    superagent.post(`${config.CDN_URL}api/v1/auth/upload`)
      .send(req.body)
      .set('accept', 'json')
      .set('Authorization', req.get('Authorization'))
      .then((result) => {
        const images = [];
        const data = result.body.images;
        data.forEach((img) => {
          images.push(`${config.CDN_URL}images/${img}`);
        });        
        res.json({ images });
      })
      .catch((error) => {
        const customError = new Error(error.response.body.message);
        customError.status = error.status;
        next(customError);
      });
  }
}

export { Images };
