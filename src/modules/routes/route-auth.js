import passport from 'passport';
import epr from 'express-promise-router';
import { Authenticate } from '../../core/authentication/aunthenticate';
import { Images } from './upload';

const router = epr();
const passportJWT = passport.authenticate('jwt', { session: false });

/**
 * Module for register api.
 */
class Api {

  static registerModule() {
    router.route('/google')
    .post(
      passport.authenticate('googleToken', { session: false }),
      Authenticate.googleOAuth
    );  
    router.route('/secret')
    .get(
      passportJWT,
      Authenticate.getUser
    );
    router.route('/upload')
    .post(
      passportJWT,
      Images.uploadImage
    );
    return router;
  }
}

export { Api };
