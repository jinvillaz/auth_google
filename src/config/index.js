require('dotenv').config();
import defaultConfig from './config.json';

const config = {
  PORT: process.env.PORT || defaultConfig.PORT,
  MONGODB_URL: process.env.MONGODB_URL || defaultConfig.MONGODB_URL,
  JWT_STRATEGY_SECRET: process.env.JWT_STRATEGY_SECRET || defaultConfig.JWT_STRATEGY_SECRET,
  OUTH_GOOGLE_CLIENT_ID: process.env.OUTH_GOOGLE_CLIENT_ID || defaultConfig.OUTH_GOOGLE_CLIENT_ID,
  OUTH_GOOGLE_CLIENT_SECRET: process.env.OUTH_GOOGLE_CLIENT_SECRET || defaultConfig.OUTH_GOOGLE_CLIENT_SECRET,
  CDN_URL: process.env.CDN_URL || defaultConfig.CDN_URL
};
export { config };
